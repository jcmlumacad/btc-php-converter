/* eslint no-unused-vars: */
'use strict';

/**
  * Sample HTML:
  * <input type="number" id="php-rate" min="0" oninput="onInputListener('php')">
  * <input type="number" id="btc-rate" min="0" oninput="onInputListener('btc')">
  */

var vm = {};
vm.php = 0;
vm.btc = 0;

var onInputListener = (function (currencyType) {
    switch (currencyType) {
        case 'php':
            var value = document.getElementById('php-rate').value;
            var computed = value * vm.btc;
            document.getElementById('btc-rate').value = computed;
            break;
        case 'btc':
            var value = document.getElementById('btc-rate').value;
            var computed = value * vm.php;
            document.getElementById('php-rate').value = computed;
            break;
        default:
            console.log('No currency available');
    }
});

(function () {
    activate();

    setInterval(activate, 30000); // Load every 30 seconds

    function activate() {
        fetch('https://quote.coins.ph/v1/markets/BTC-PHP')
            .then(function (response) {
                var result = response.json();
                result.then(function (data) {
                    var market = data.market;
                    vm.php = parseInt(market.ask);
                    vm.btc = 1 / market.ask;
                })
            })
            .catch(function (error) {
                console.error('Error', error);
            });
    }
})();
